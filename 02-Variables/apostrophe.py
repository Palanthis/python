##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the BSD 2-clause
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
# Correctly encased in double quotes.
message = "One of Python's strengths is its diverse community"
print(message)

# With single quotes, should throw an error.
message = 'One of Python's strengths is its diverse community'
print(message)