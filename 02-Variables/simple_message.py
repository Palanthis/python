##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the BSD 2-clause
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
#
var1 = "This is my custom message."
print(var1)

var2 = "This is my second message, using a different variable name."
print(var2)