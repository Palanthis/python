##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the BSD 2-clause
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
# Personal Message
name = "Eric"
print(f"Hello, {name.title()}. Would you like to learn some Python today?")

# Name Cases
fname = "bob"
lname = "smith"
full_name = f"{fname} {lname}"
print(full_name)
print(full_name.title())
print(full_name.upper())
print(full_name.lower())

# Famous Quote
quote = "Life is hard. It's even harder if you're stupid."
famous_person = "John Wayne"
print(quote)
print(famous_person)

# Stripping Names
name = "  Sam Smith  "
print(name)
print(name.lstrip())
print(name.rstrip())
print(name.strip())

# Lines and tabs
var_1 = "Bogus Data"
var_2 = "Hokum Info"