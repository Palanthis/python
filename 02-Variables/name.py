##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://gitlab.com/Palanthis
# License	:	Distributed under the BSD 2-clause
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
#
name = "ada lovelace"
print(name.title())

name = "ada lovelace"
print(name.upper())

name = "ADA LOVELACE"
print(name.lower())